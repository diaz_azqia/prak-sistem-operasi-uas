# 2. Mampu mendemonstrasikan pembuatan Docker Image melalui Dockerfile, sebagai bentuk implementasi dari sistem operasi.

Untuk membuat Docker Image menggunakan Dockerfile bisa sebagai berikut:


1. Base Image: Pertama, Dockerfile dimulai dengan  FROM debian:bullseye-slim.


2. Install Dependensi: Selanjutnya,RUN apt-get update && apt-get install -y git neovim netcat python3-pip  untuk mengupdate paket dan menginstal dependensi yang diperlukan dalam kontainer. Dependensi yang diinstal termasuk git, neovim, netcat, dan python3-pip.


3. Copy File Aplikasi: Pernyataan COPY hadits-d.py /home/hadits-digital/hadits-d.py dan COPY requirements.txt /home/hadits-digital/requirements.txt digunakan untuk menyalin file aplikasi warung.py dan file requirements.txt ke dalam kontainer. File aplikasi dan file dependensi ini akan digunakan dalam langkah-langkah selanjutnya.


4. Install Dependensi Python: RUN pip3 install --no-cache-dir -r /home/warung.py/requirements.txt digunakan untuk menginstal dependensi Python yang diperlukan dalam aplikasi. File requirements.txt berisi daftar dependensi Python yang dibutuhkan oleh aplikasi.


5. Set Working Directory: Pernyataan WORKDIR /home/hadits-digital menetapkan direktori kerja di dalam kontainer sebagai /home/warung.py. Ini adalah direktori di mana file-file aplikasi dan dependensi akan berada.


6. Expose Port: Pernyataan EXPOSE 25026 digunakan untuk mengekspos port 25026 di dalam kontainer. Port ini akan digunakan untuk mengakses aplikasi Thrifting Pakaian yang berjalan dalam kontainer.


7. Start Web Service: Pernyataan CMD ["streamlit", "run", "--server.port=25026", "warung.py"] digunakan untuk menjalankan perintah saat kontainer dimulai. Pada kasus ini, perintah streamlit run --server.port=25026 warung.py akan dijalankan untuk menjalankan aplikasi Streamlit.


Setelah Dockerfile selesai, Anda dapat menggunakan perintah docker build untuk membangun Docker Image dari Dockerfile tersebut. 

# 3. Mampu mendemonstrasikan pembuatan web page / web service sederhana berdasarkan permasalahan dunia nyata yang dijalankan di dalam Docker Container

Langkah-langkah:

Buat direktori baru untuk proyek dan masuk ke dalamnya.
Buat file Dockerfile untuk mendefinisikan Docker image yang akan digunakan.
Buat file requirements.txt untuk mengelola dependensi Python yang diperlukan.
Buat file warung-bayu yang akan berisi kode aplikasi web.

Konfigurasi Docker:

1. Membuat file Dockerfile:

```
FROM debian:bullseye-slim

#install
RUN apt-get update && apt-get install -y git neovim netcat python3-pip

#Copy web service files
COPY alfagift.py /home/alfa-gift/alfagift.py
COPY requirements.txt /home/alfa-gift/requirements.txt

#install python
RUN pip3 install --no-cache-dir -r /home/alfa-gift/requirements.txt

#set working directory
WORKDIR /home/alfa-gift

#expose port
EXPOSE 25038

#start web service
CMD ["streamlit", "run", "--server.port=25038", "alfagift.py"]
```

2. Membuat file alfagift.py

```
import streamlit as st

# Daftar mobil dan harganya
daftarMobil = [
    {"Merk": "Toyota", "Model": "Avanza", "Harga": 150000000},
    {"Merk": "Honda", "Model": "City", "Harga": 180000000},
    {"Merk": "Suzuki", "Model": "Ertiga", "Harga": 170000000},
    {"Merk": "Mitsubishi", "Model": "Xpander", "Harga": 200000000},
    {"Merk": "Nissan", "Model": "Livina", "Harga": 160000000}
]

# Fungsi untuk menampilkan daftar mobil
def tampilkanDaftarMobil(daftarMobil):
    st.write("Daftar Mobil:")
    st.write("================")
    for mobil in daftarMobil:
        st.write("Merk:", mobil["Merk"])
        st.write("Model:", mobil["Model"])
        st.write("Harga: Rp{:,.0f}".format(mobil["Harga"]))
        st.write("-----------------")

# Fungsi untuk mencari mobil berdasarkan merk dan model
def cariMobil(daftarMobil, merk, model):
    for mobil in daftarMobil:
        if mobil["Merk"].lower() == merk.lower() and mobil["Model"].lower() == model.lower():
            return mobil
    return None

# Fungsi untuk melakukan pembelian mobil
def beliMobil(mobil, uang):
    if uang >= mobil["Harga"]:
        kembalian = uang - mobil["Harga"]
        st.write("Pembelian berhasil!")
        st.write("Kembalian Anda: Rp{:,.0f}".format(kembalian))
    else:
        st.write("Maaf, uang Anda tidak cukup untuk membeli mobil ini.")

# Fungsi untuk menambahkan uang
def tambahUang(uang):
    tambahan = st.number_input("Masukkan jumlah uang yang ingin ditambahkan:", min_value=0, step=1000000)
    uang += tambahan
    st.write("Uang berhasil ditambahkan.")
    st.write("Total uang Anda sekarang: Rp{:,.0f}".format(uang))
    return uang

# Tampilan awal
tampilkanDaftarMobil(daftarMobil)

# Main program
uang = st.sidebar.number_input("Masukkan jumlah uang Anda:", min_value=0, step=1000000)
merk_mobil = st.sidebar.text_input("Masukkan merk mobil yang ingin Anda beli:")
model_mobil = st.sidebar.text_input("Masukkan model mobil yang ingin Anda beli:")

if st.sidebar.button("Beli"):
    mobil = cariMobil(daftarMobil, merk_mobil, model_mobil)
    if mobil is not None:
        st.write("Mobil ditemukan!")
        st.write("Merk:", mobil["Merk"])
        st.write("Model:", mobil["Model"])
        st.write("Harga: Rp{:,.0f}".format(mobil["Harga"]))
        if uang >= mobil["Harga"]:
            beliMobil(mobil, uang)
        else:
            st.write("Maaf, uang Anda tidak cukup untuk membeli mobil ini.")
            pilihan = st.sidebar.radio("Pilihan selanjutnya:", ("Tambah Uang", "Batal"))
            if pilihan == "Tambah Uang":
                uang = tambahUang(uang)
            else:
                st.write("Terima kasih!")
```


# 4. Mampu mendemonstrasikan penggunaan Docker Compose pada port tertentu sebagai bentuk kontainerisasi program dan sistem operasi yang menjadi dasar distributed computing.

- Buat direktori baru untuk proyek dan masuk ke dalamnya.
Buat file docker-compose.yml untuk mendefinisikan layanan yang akan dijalankan.

Konfigurasi Docker Compose:

Membuat file docker-compose.yml:

```
version: "3"
services:
  alfa-gift:
    image: diazqi13/alfa-gift:1.0
    ports:
    - "25038:25038"
    networks:
    - niat-yang-suci
    volumes:
    - /home/praktikumc/1217050038/alfa-gift:/alfa-gift

  networks:
    niat-yang-suci:
      external: true
```

# 5. Mampu menjelaskan project yang dibuat dalam bentuk file README.md

Deskripsi project
Program tersebut adalah sebuah aplikasi sederhana yang menggunakan framework Streamlit dan library Pandas di Python untuk membuat tampilan interaktif yang memungkinkan pengguna untuk mencari dan menampilkan jenis mobil yang di inginkan.


- Import Library

`import streamlit as st`

Kode di atas mengimpor  library yang digunakan dalam program ini, yaitu streamlit.streamlit adalah library yang digunakan untuk membuat antarmuka web.

- Daftar Mobil dan Harganya

```
daftarMobil = [
    {"Merk": "Toyota", "Model": "Avanza", "Harga": 150000000},
    {"Merk": "Honda", "Model": "City", "Harga": 180000000},
    {"Merk": "Suzuki", "Model": "Ertiga", "Harga": 170000000},
    {"Merk": "Mitsubishi", "Model": "Xpander", "Harga": 200000000},
    {"Merk": "Nissan", "Model": "Livina", "Harga": 160000000}
```

Variabel daftarBarang adalah daftar mobil yang tersedia. Setiap barang direpresentasikan sebagai sebuah dictionary dengan kunci `Merk` beserta `Harga` yang ada

- Fungsi tampilkanDaftarMobil

```
def tampilkanDaftarMobil(daftarMobil):
    st.write("Daftar Mobil:")
    st.write("================")
    for mobil in daftarMobil:
        st.write("Merk:", mobil["Merk"])
        st.write("Model:", mobil["Model"])
        st.write("Harga: Rp{:,.0f}".format(mobil["Harga"]))
        st.write("-----------------")
```

Fungsi ini digunakan untuk menampilkan daftar Mobil ke Interface menggunakan streamlit.

- Fungsi untuk mencari mobil berdasarkan merk dan model

```
def cariMobil(daftarMobil, merk, model):
    for mobil in daftarMobil:
        if mobil["Merk"].lower() == merk.lower() and mobil["Model"].lower() == model.lower():
            return mobil
    return None
```

Fungsi ini digunakan untuk menampilkan daftar mobil ke Interface dengan cara mencari mobil bedasarkan nama.

- Fungsi untuk melakukan pembelian mobil

```
def beliMobil(mobil, uang):
    if uang >= mobil["Harga"]:
        kembalian = uang - mobil["Harga"]
        st.write("Pembelian berhasil!")
        st.write("Kembalian Anda: Rp{:,.0f}".format(kembalian))
    else:
        st.write("Maaf, uang Anda tidak cukup untuk membeli mobil ini.")
```

Fungsi ini untuk melakukan pembelian

- Fungsi untuk menambahkan uang

```
def tambahUang(uang):
    tambahan = st.number_input("Masukkan jumlah uang yang ingin ditambahkan:", min_value=0, step=1000000)
    uang += tambahan
    st.write("Uang berhasil ditambahkan.")
    st.write("Total uang Anda sekarang: Rp{:,.0f}".format(uang))
    return uang
```

Fungsi ini untuk top up atau menambahkan uang user

- Tampilan awal

`tampilkanDaftarMobil(daftarMobil)`

Ini menampilkan daftar mobil saat program dijalankan.

- Main program

```
uang = st.sidebar.number_input("Masukkan jumlah uang Anda:", min_value=0, step=1000000)
merk_mobil = st.sidebar.text_input("Masukkan merk mobil yang ingin Anda beli:")
model_mobil = st.sidebar.text_input("Masukkan model mobil yang ingin Anda beli:")

if st.sidebar.button("Beli"):
    mobil = cariMobil(daftarMobil, merk_mobil, model_mobil)
    if mobil is not None:
        st.write("Mobil ditemukan!")
        st.write("Merk:", mobil["Merk"])
        st.write("Model:", mobil["Model"])
        st.write("Harga: Rp{:,.0f}".format(mobil["Harga"]))
        if uang >= mobil["Harga"]:
            beliMobil(mobil, uang)
        else:
            st.write("Maaf, uang Anda tidak cukup untuk membeli mobil ini.")
            pilihan = st.sidebar.radio("Pilihan selanjutnya:", ("Tambah Uang", "Batal"))
            if pilihan == "Tambah Uang":
                uang = tambahUang(uang)
            else:
                st.write("Terima kasih!")
```
Variabel uang digunakan untuk melacak jumlah uang yang dimiliki oleh pengguna. Variabel menu menyimpan pilihan menu yang dipilih pengguna melalui sidebar streamlit. Program kemudian memeriksa pilihan menu tersebut dan menjalankan fungsi yang sesuai:

1. Jika menu "Tambah Uang" dipilih, fungsi tambahUang() akan dieksekusi.
2. Jika menu "Cari Barang" dipilih, pengguna diminta memasukkan nama barang yang ingin dicari melalui input teks. Jika nama barang valid, fungsi cariBarang() akan dijalankan untuk mencari barang tersebut.
3. Jika menu "Panggil API" dipilih, fungsi panggilAPI() akan dijalankan untuk memanggil API.
4. Jika menu "Selesai" dipilih, program akan menampilkan pesan penutup.

Setelah fungsi yang sesuai dijalankan, program akan tetap berjalan dan menunggu interaksi pengguna melalui antarmuka web yang dihasilkan oleh streamlit.

- Penjelasan bagaimana sistem operasi digunakan dalam proses containerization:

Dalam proses containerization, sistem operasi digunakan dalam beberapa cara:

1. Isolasi Sumber Daya: Sistem operasi menyediakan mekanisme untuk mengisolasi sumber daya antara container yang berjalan di lingkungan yang sama. Ini memungkinkan setiap container berjalan secara terpisah, dengan sumber daya yang terisolasi seperti CPU, memori, jaringan, dan sistem file. Isolasi ini mencegah saling interferensi antar-container dan memastikan bahwa setiap container memiliki lingkungan yang terpisah dan aman.

2. Pengaturan Jaringan: Sistem operasi memfasilitasi pengaturan jaringan untuk setiap container. Ini melibatkan penerusan port, pengaturan jaringan internal antara container, serta koneksi jaringan dengan host atau container lain. Sistem operasi memungkinkan konfigurasi jaringan yang fleksibel dan dapat disesuaikan sesuai dengan kebutuhan aplikasi yang berjalan di dalam container.

3. Manajemen Sumber Daya: Sistem operasi berperan dalam manajemen sumber daya seperti alokasi CPU, memori, dan ruang disk untuk setiap container. Dengan mengatur batasan sumber daya ini, sistem operasi memastikan bahwa setiap container mendapatkan sumber daya yang diperlukan dan tidak berlebihan. Sistem operasi juga mengelola sumber daya secara efisien dengan memprioritaskan penggunaan sumber daya antara container yang berjalan di lingkungan yang sama.

4. Pemutakhiran dan Perbaikan: Sistem operasi yang digunakan dalam container dapat diperbarui atau diperbaiki secara terpisah tanpa mempengaruhi kontainer lainnya. Ini memungkinkan pemeliharaan dan pembaruan yang lebih mudah dan cepat. Sistem operasi yang terpisah dalam setiap container juga memungkinkan penggunaan versi yang berbeda dari sistem operasi yang sama dalam lingkungan yang sama.

5. Interaksi dengan Container Runtime: Sistem operasi berinteraksi dengan container runtime seperti Docker atau Kubernetes untuk mengelola dan menjalankan kontainer. Sistem operasi menyediakan antarmuka dan mekanisme untuk membuat, mengelola, dan mengontrol kontainer. Container runtime menggunakan fitur-fitur yang disediakan oleh sistem operasi untuk mengisolasi dan menjalankan kontainer dengan efisien.

Dalam containerization, sistem operasi yang umum digunakan adalah Linux, dengan kernel yang mendukung fitur-fitur container seperti namespaces, kontrol grup, dan alat manajemen sumber daya. Linux memungkinkan pembuatan dan pengelolaan kontainer yang efisien dan aman. Selain itu, ada juga sistem operasi khusus yang dirancang khusus untuk lingkungan kontainer seperti CoreOS atau RancherOS.

Secara keseluruhan, sistem operasi berperan penting dalam proses containerization dengan menyediakan isolasi sumber daya, pengaturan jaringan, manajemen sumber daya, pemutakhiran dan perbaikan, serta interaksi dengan container runtime. Hal ini memungkinkan penggunaan kontainer yang efisien, aman, dan dapat diandalkan untuk menjalankan aplikasi dalam lingkungan terisolasi.

- Penjelasan bagaimana containerization dapat membantu mempermudah pengembangan aplikasi:

Containerization dapat membantu mempermudah pengembangan aplikasi dengan beberapa cara:

1. Lingkungan yang Konsisten: Dengan menggunakan container, pengembang dapat menciptakan lingkungan yang konsisten di seluruh siklus pengembangan. Kontainer menyediakan paket yang lengkap dengan semua dependensi dan konfigurasi yang diperlukan untuk menjalankan aplikasi. Ini memastikan bahwa aplikasi berjalan dengan konsisten di berbagai lingkungan, mulai dari lingkungan pengembangan lokal hingga produksi.

2. Portabilitas: Container dapat dengan mudah dipindahkan di antara lingkungan pengembangan, pengujian, dan produksi. Kontainer berisi semua dependensi yang diperlukan dan dapat dijalankan di berbagai platform, sistem operasi, atau infrastruktur cloud. Portabilitas ini memungkinkan pengembang untuk dengan cepat menguji dan menerapkan aplikasi di berbagai lingkungan tanpa khawatir tentang perbedaan konfigurasi atau dependensi yang hilang.

3. Skalabilitas dan Elastisitas: Container memungkinkan pengembang untuk dengan mudah mengelola skalabilitas dan elastisitas aplikasi. Kontainer dapat dengan cepat diubah ukurannya, ditingkatkan, atau dikurangi sesuai dengan permintaan beban kerja. Ini memungkinkan pengembang untuk menangani lonjakan lalu lintas atau beban kerja yang tinggi dengan cepat dan efisien, sehingga aplikasi tetap responsif dan tersedia bagi pengguna.

4. Isolasi dan Keamanan: Setiap kontainer berjalan dalam lingkungan terisolasi yang terpisah dari kontainer lainnya dan host. Isolasi ini memungkinkan aplikasi berjalan dengan aman tanpa saling mempengaruhi. Kontainer juga dapat memiliki kebijakan akses yang didefinisikan dengan jelas, membatasi akses ke sumber daya atau jaringan yang tidak diperlukan. Hal ini membantu menjaga keamanan aplikasi dan melindungi data sensitif.

5. Pengembangan Kolaboratif: Container memfasilitasi kolaborasi tim yang lebih baik dalam pengembangan aplikasi. Dengan menggunakan kontainer, tim pengembang dapat membagikan dan mereplikasi lingkungan pengembangan yang serupa. Hal ini memungkinkan pengembang untuk berbagi kode, konfigurasi, dan lingkungan dengan mudah, sehingga memfasilitasi kolaborasi, debugging, dan pengujian bersama.

6. Proses Pengiriman yang Cepat: Container mempercepat proses pengiriman aplikasi. Kontainer dapat dengan cepat dibangun, diuji, dan didistribusikan ke lingkungan produksi. Proses ini memperpendek waktu yang dibutuhkan untuk menyampaikan perubahan dan fitur baru ke pengguna. Dalam kombinasi dengan praktik CI/CD (Continuous Integration/Continuous Deployment), pengembang dapat melakukan pengiriman aplikasi dengan cepat dan sering.

Secara keseluruhan, containerization mempermudah pengembangan aplikasi dengan menyediakan lingkungan yang konsisten, portabilitas, skalabilitas, isolasi dan keamanan, kolaborasi tim yang lebih baik, serta proses pengiriman yang cepat. Ini memungkinkan pengembang untuk lebih fokus pada pengembangan aplikasi itu sendiri dan mengurangi kompleksitas terkait infrastruktur dan konfigurasi.

- Penjelasan apa itu DevOps, bagaimana DevOps membantu pengembangan aplikasi:

DevOps adalah pendekatan dalam pengembangan perangkat lunak yang menggabungkan praktik-praktik pengembangan (Development) dan operasional (Operations) secara terintegrasi. DevOps bertujuan untuk menciptakan alur kerja yang efisien, berkelanjutan, dan berkolaborasi antara tim pengembangan dan tim operasional.

DevOps membantu pengembangan aplikasi dengan beberapa cara:

1. Kolaborasi Tim yang Kuat: DevOps mendorong kolaborasi yang erat antara tim pengembangan dan tim operasional. Ini memungkinkan komunikasi yang lebih baik, pemahaman yang lebih mendalam tentang kebutuhan bisnis, dan koordinasi yang efisien dalam menghadapi tantangan dan perubahan dalam pengembangan aplikasi. Kolaborasi yang kuat memungkinkan pengembang untuk memahami persyaratan operasional dan memastikan bahwa aplikasi dapat diimplementasikan dan dikelola dengan lancar.

2. Automasi Proses dan Pengujian: DevOps menganjurkan otomatisasi dalam berbagai aspek pengembangan aplikasi. Automasi dapat diterapkan dalam tahap pembangunan, pengujian, pengiriman, dan pemeliharaan aplikasi. Dengan otomatisasi, tugas-tugas berulang dapat diatasi dengan cepat, memungkinkan pengembang untuk fokus pada inovasi dan pengembangan fitur baru. Automasi juga membantu memastikan kualitas aplikasi melalui pengujian otomatis yang terintegrasi.

3. Penerapan Continuous Integration/Continuous Deployment (CI/CD): DevOps mendorong penerapan CI/CD, yaitu pendekatan di mana perubahan kode secara terus-menerus diuji, diintegrasikan, dan diimplementasikan ke lingkungan produksi secara otomatis. CI/CD memungkinkan pengembang untuk dengan cepat menerapkan perubahan, mengurangi waktu penyebaran, dan mengurangi risiko kesalahan. Dengan adopsi CI/CD, pengembangan aplikasi menjadi lebih responsif dan dapat memenuhi kebutuhan pasar yang berubah dengan cepat.

4. Monitoring dan Tindak Lanjut: DevOps menekankan pentingnya pemantauan aplikasi secara terus-menerus dan respons cepat terhadap masalah yang muncul. Dengan menggunakan alat pemantauan dan logging yang tepat, tim DevOps dapat mendapatkan wawasan tentang kinerja aplikasi dan melakukan tindakan yang diperlukan untuk mengatasi masalah sebelum berdampak pada pengguna. Dengan demikian, DevOps membantu memastikan ketersediaan dan keandalan aplikasi secara terus-menerus.

Secara keseluruhan, DevOps membantu pengembangan aplikasi dengan mengintegrasikan tim dan alur kerja, menerapkan otomasi, mengadopsi CI/CD, meningkatkan monitoring dan tindak lanjut, serta mengubah budaya dan mindset. Dengan pendekatan ini, pengembang dapat mengurangi waktu siklus pengembangan, meningkatkan kualitas, dan memberikan aplikasi yang lebih responsif dan andal kepada pengguna.

- Berikan contoh kasus penerapan DevOps di perusahaan / industri dunia nyata (contoh bagaimana implementasi DevOps di Gojek, dsb.)

Salah satu contoh penerapan DevOps di industri dunia nyata adalah di perusahaan teknologi asal Indonesia, Gojek. Gojek adalah platform transportasi dan layanan serba ada yang menyediakan berbagai layanan seperti pengiriman makanan, ojek online, pembayaran digital, dan banyak lagi. Di sini, saya akan memberikan contoh bagaimana implementasi DevOps dilakukan di Gojek:

1. Kolaborasi Tim: Gojek menerapkan kolaborasi yang erat antara tim pengembangan perangkat lunak dan tim operasional. Tim-tim ini bekerja bersama dalam siklus pengembangan perangkat lunak yang terpadu, berkomunikasi secara terbuka, dan berbagi pengetahuan. Kolaborasi yang kuat memungkinkan mereka untuk memahami kebutuhan bisnis, menerapkan perubahan dengan cepat, dan menyelesaikan masalah operasional dengan efisien.

2. Automasi Proses dan Pengujian: Gojek menerapkan otomatisasi dalam berbagai tahap pengembangan perangkat lunak. Mereka menggunakan alat otomatisasi untuk membangun, menguji, dan menerapkan perubahan secara otomatis. Hal ini mempercepat siklus pengembangan, mengurangi risiko kesalahan, dan memastikan kualitas perangkat lunak yang tinggi.

3. Continuous Integration/Continuous Deployment (CI/CD): Gojek menerapkan pendekatan CI/CD yang memungkinkan mereka untuk secara terus-menerus menguji, mengintegrasikan, dan mendeploy perubahan ke lingkungan produksi secara otomatis. Dengan adanya CI/CD, Gojek dapat dengan cepat menerapkan perbaikan dan fitur baru ke platform mereka, sehingga memberikan pengalaman yang lebih baik bagi pengguna.

4. Monitoring dan Tindak Lanjut: Gojek sangat memperhatikan pemantauan aplikasi mereka secara real-time. Mereka menggunakan alat pemantauan yang canggih untuk mengawasi kinerja dan ketersediaan platform mereka. Tim DevOps di Gojek siap untuk merespons masalah yang muncul dengan cepat dan melakukan tindakan perbaikan yang diperlukan untuk memastikan platform tetap berjalan dengan lancar.

5. Infrastruktur sebagai Kode (Infrastructure as Code): Gojek menggunakan prinsip Infrastruktur sebagai Kode untuk mengelola dan mengkonfigurasi infrastruktur mereka secara otomatis. Mereka menggunakan alat-alat seperti Terraform dan Ansible untuk menyediakan dan mengelola sumber daya infrastruktur yang diperlukan. Pendekatan ini memastikan konsistensi dan ketepatan dalam pengaturan infrastruktur mereka.

Melalui penerapan DevOps, Gojek berhasil menciptakan alur kerja yang efisien, berkelanjutan, dan berkolaborasi antara tim pengembangan dan tim operasional. Hal ini memungkinkan mereka untuk mempercepat pengembangan perangkat lunak, meningkatkan kualitas dan ketersediaan platform, serta memberikan pengalaman yang unggul kepada pengguna mereka.

Perusahaan lain seperti Netflix, Amazon, dan Google juga menerapkan prinsip-prinsip DevOps dengan sukses untuk menghadapi kompleksitas dan skala operasional yang besar dalam industri teknologi.



# 6. Mampu menjelaskan dan mendemonstrasikan web page / web service yang telah dapat diakses public dalam bentuk video Youtube.

# 7. Mampu mempublikasikan Docker Image yang telah dibuat ke Docker Hub, untuk memudahkan penciptaan container sistem operasi di berbagai server pada distributed computing.

![ss](ss/ssdocker.png)

https://hub.docker.com/repository/docker/diazqi13/alfa-gift/general
